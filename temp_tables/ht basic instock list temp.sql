
CREATE TABLE HT_SANDBOX_DEV_DB.ALLOC_TMP.HT_BASIC_INSTOCK_LIST_TEMP (SKU_NUM VARCHAR(50),
                                                                    SIZE VARCHAR(50),
                                                                    SIZE_INDEX INTEGER,
                                                                    CORE VARCHAR(50), 
                                                                    THRESHOLD INTEGER,
                                                                    COLLECTION_ID VARCHAR(300),
                                                                    LAYER_SKU VARCHAR(50),
                                                                    ACTIVE VARCHAR(50),
                                                                    UPDATED_DTTM DATETIME,
                                                                    CREATED_DTTM DATETIME,
                                                                    WEB_FLAG INTEGER,
                                                                    STORE_FLAG INTEGER)