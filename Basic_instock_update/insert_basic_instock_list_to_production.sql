INSERT INTO HT_SANDBOX_DEV_DB.ALLOC_DWH.HT_DWH_F_BASIC_INSTOCK_LU(ITM_KEY,
                                                                  SKU_NUM,
                                                                  SIZE,
                                                                  SIZE_INDEX,
                                                                  CORE,
                                                                  THRESHOLD,
                                                                  COLLECTION_ID,
                                                                  LAYER_SKU,
                                                                  ACTIVE,
                                                                  UPDATED_DTTM,
                                                                  CREATED_DTTM,
                                                                  WEB_FLAG,
                                                                  STORE_FLAG)

SELECT TB2.ITM_KEY,
    TB1.SKU_NUM,
    TB1.SIZE,
    TB1.SIZE_INDEX,
    TB1.CORE,
    TB1.THRESHOLD,
    TB1.COLLECTION_ID,
    TB1.LAYER_SKU,
    TB1.ACTIVE,
    TB1.UPDATED_DTTM,
    TB1.CREATED_DTTM,
    TB1.WEB_FLAG,
    TB1.STORE_FLAG
FROM HT_SANDBOX_DEV_DB.ALLOC_TMP.HT_BASIC_INSTOCK_LIST_TEMP TB1
INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_PRD_ITM_LU TB2
    ON TB2.ITM_ID = TB1.SKU_NU