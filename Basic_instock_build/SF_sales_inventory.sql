
WITH 
--GETTING LIST OF ALL STYLES
--NEED TO ADD TABLE OF STYLES FOR BASIC INSTOCK REPORT. 
ITM_LIST  as ( SELECT DISTINCT ITM_KEY,
                    ITM_ID,
                    ITM_DESC,
                    STY_ID,
                    STY_DESC,
                    -- WHAT MASTER STYLE
                    SIZE_ID,
                    SIZE_DESC,
                    DPT_KEY,
                    DPT_ID,
                    DPT_DESC,
                    CLS_KEY,
                    CLS_ID,
                    CLS_DESC,
                    SBC_KEY,
                    SBC_ID,
                    SBC_DESC,
                    LICENSE,
                    SUB_LICENSE,
                    PROPERTY_TYPE,
                    BAND,
                    MUSIC_GENRE,
                    MUSIC_SUPER_GENRE,
                    BODY_STYLE_SILHOUETTE
                FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_PRD_ITM_UDA_LU
                WHERE ITM_KEY IN (SELECT ITM_KEY 
                                  FROM HT_SANDBOX_DEV_DB.ALLOC_DWH.HT_DWH_F_BASIC_INSTOCK_LU
                                  where active= 'True')),
    
--GETTING LIST OF STORE NUMBERS
STORE_LIST AS ( SELECT DISTINCT LOC_KEY,
                LOC_ID ,
                COMPANY_ID,
                RGN_ID,
                RGN_DESC,
                LOC_STATE_CDE,
                STR_TYP_CDE_DESC,
                LOC_DESC
                FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_ORG_LOC_LU
                   WHERE CHN_KEY = '1'  
                    AND LOC_KEY IN (SELECT DISTINCT LOC_KEY 
                                    FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_HT_DWH_D_ORG_STR_ATTR_LU 
                                    WHERE ACTIVE_LOC = 'Y')
                    ORDER BY LOC_ID),           

--GETTING WEEK INFORMATION
week_list as ( SELECT DISTINCT WK_KEY 
                FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV
                WHERE DAY_KEY between DATEADD(dd,-7,CURRENT_DATE()) AND CURRENT_DATE()),
                
SLS_LIST as (SELECT DISTINCT WK_KEY,
             LOC_KEY,
             ITM_KEY,
             F_SLS_QTY,
             F_SLS_RTL_LCL
             FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_HT_DWH_F_SLS_TXN_ITM_W
              WHERE LOC_KEY IN (SELECT LOC_KEY FROM STORE_LIST) 
              AND WK_KEY IN (SELECT DISTINCT WK_KEY FROM week_list)
              AND ITM_KEY IN (SELECT DISTINCT ITM_KEY FROM ITM_LIST)),
              
INV_LIST AS (SELECT DISTINCT WK_KEY,
                LOC_KEY,
                ITM_KEY, 
                F_EOH_QTY,
                F_EOH_RTL,
                F_EOP_IT_RTL_LCL
              FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_F_INV_EOH_ILW_B
                WHERE F_EOH_QTY <> 0 
                AND LOC_KEY IN (SELECT LOC_KEY FROM STORE_LIST) 
                AND WK_KEY IN (SELECT DISTINCT WK_KEY FROM week_list)
                AND ITM_KEY IN (SELECT DISTINCT ITM_KEY FROM ITM_LIST)),
                
AGG_TABLE AS (SELECT DISTINCT WK_KEY, LOC_KEY, ITM_KEY
                  FROM (SELECT WK_KEY,LOC_KEY,ITM_KEY
                        FROM INV_LIST 
                          UNION ALL 
                        SELECT WK_KEY,LOC_KEY,ITM_KEY
                        FROM SLS_LIST))
                   
                
SELECT * 
FROM ( SELECT REF.WK_KEY, 
              REF.LOC_KEY, 
              REF.ITM_KEY,
              SL.LOC_ID, 
              PROD.ITM_ID AS sku_number,
              PROD.STY_ID as master_style, 
              PROD.STY_DESC as parent_style_desc,
              PROD.SIZE_DESC AS size,
              PROD.DPT_KEY AS department,
              PROD.DPT_DESC as department_name,
              PROD.CLS_ID AS class,
              PROD.CLS_DESC AS class_name,
              PROD.SBC_ID AS subclass,
              PROD.SBC_DESC AS subclass_name,
              PROD.LICENSE as license,
              PROD.SUB_LICENSE as sub_license,
              PROD.BAND as band,
              PROD.MUSIC_GENRE as music_genre,
              PROD.MUSIC_SUPER_GENRE as music_super_genre,
              PROD.PROPERTY_TYPE AS property_type, 
              PROD.BODY_STYLE_SILHOUETTE AS body_style_silhouette,
              INV.F_EOH_QTY AS unit_eop,
              INV.F_EOH_RTL,
              INV.F_EOP_IT_RTL_LCL AS reg_c_eop,
              SLS.F_SLS_QTY AS unit_sales,
              SLS.F_SLS_RTL_LCL AS regpro_retail_sales,
              BIS.WEB_FLAG AS web_flag,
              BIS.STORE_FLAG as store_flag,
              CASE WHEN BIS.WEB_FLAG = '0' AND SL.LOC_ID < 3000 THEN 'Keep'
                   WHEN BIS.STORE_FLAG = '0' AND SL.LOC_ID > 3000 THEN 'Keep'
                   WHEN BIS.STORE_FLAG = '1' AND BIS.STORE_FLAG = '1' THEN 'Keep'
              END as operation  
      FROM AGG_TABLE as REF
      INNER JOIN STORE_LIST AS SL
        ON SL.LOC_KEY = REF.LOC_KEY
      INNER JOIN ITM_LIST AS PROD
        ON PROD.ITM_KEY = REF.ITM_KEY
      FULL OUTER JOIN INV_LIST AS INV
       ON INV.WK_KEY = REF.WK_KEY
       AND INV.LOC_KEY = REF.LOC_KEY
       AND INV.ITM_KEY = REF.ITM_KEY
      FULL OUTER JOIN SLS_LIST AS SLS
        ON SLS.WK_KEY = REF.WK_KEY
        AND SLS.LOC_KEY = REF.LOC_KEY
        AND SLS.ITM_KEY = REF.ITM_KEY
       INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH.HT_DWH_F_BASIC_INSTOCK_LU AS BIS
        ON BIS.ITM_KEY = REF.ITM_KEY) as dt
WHERE operation IS NOT NULL

               
  
