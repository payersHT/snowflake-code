-- Ignore the "IF OBJECT_ID statements. They exist to clean up temp tables."
IF OBJECT_ID('tempdb..#selected_product_init','U') IS NOT NULL
       DROP TABLE #selected_product_init

IF OBJECT_ID('tempdb..#selected_product','U') IS NOT NULL
       DROP TABLE #selected_product

IF OBJECT_ID('tempdb..#week','U') IS NOT NULL
       DROP TABLE #week

IF OBJECT_ID('tempdb..#inv','U') IS NOT NULL
       DROP TABLE #inv

IF OBJECT_ID('tempdb..#sls','U') IS NOT NULL
       DROP TABLE #sls

IF OBJECT_ID('tempdb..#store_m','U') IS NOT NULL
       DROP TABLE #store_m

IF OBJECT_ID('tempdb..#week_store_sku_size','U') IS NOT NULL
       DROP TABLE #week_store_sku_size


DECLARE @start_date date = DATEADD(DAY,-7 ,CONVERT(date, SYSDATETIME()));
DECLARE @end_date date = CONVERT(date, SYSDATETIME()); 

SELECT DISTINCT sku_number
        ,sku_description
        ,style_number
        ,master_style
        ,style_description
        ,size
        ,department
        ,department_name
        ,class
        ,class_name
        ,subclass
        ,sub_name
        ,license
        ,sub_license
        ,property_type
        ,band
        ,music_genre
        ,music_super_genre
        ,body_style_silhouette
INTO #selected_product_init
FROM [SizingAnalysis].[dbo].[dim_product]
WHERE master_style IN (SELECT DISTINCT master_style FROM [PAModelData].[dbo].[ht_basic_instock_list] WHERE active = '1') ;

SELECT DISTINCT [STORE_NUM]
      ,[LOC_TYPE]
      ,[COMPANY]
      ,[ACTIVE]
      ,[DIV]
      ,[REG]
      ,[RD]
      ,[DIST]
      ,[DM]
      ,[NAME]
      ,[LOCATION NAME]
      ,[CITY]
      ,[STATE]
INTO #store_m
FROM [SizingAnalysis].[dbo].[store_master]
WHERE div = 1;

SELECT DISTINCT init_a.sku_number
        ,init_a.sku_description
        ,init_a.style_number
        ,init_a.style_description
        ,init_a.master_style
        ,init_b.style_description parent_style_desc
        ,init_a.size
        ,init_a.department
        ,init_a.department_name
        ,init_a.class
        ,init_a.class_name
        ,init_a.subclass
        ,init_a.sub_name
        ,init_a.license
        ,init_a.sub_license
        ,init_a.property_type
        ,init_a.band
        ,init_a.music_genre
        ,init_a.music_super_genre
        ,init_a.body_style_silhouette
INTO #selected_product
FROM #selected_product_init init_a 
JOIN #selected_product_init init_b
ON init_a.master_style = init_b.style_number;

SELECT DISTINCT week
INTO #week
FROM [SizingAnalysis].[dbo].[dim_time]
WHERE day_dt BETWEEN @start_date AND @end_date;

SELECT [fiscalweek]
    ,[store_number]
    ,[sku_number]
    ,[style_number]
    ,[size]
    ,[unit_eop]
    ,[reg_unit_eop]
    ,[md_u_eop]
    ,[c_eop]
    ,[reg_c_eop]
    ,[md_c_eop]
    ,[r_eop]
    ,[reg_r_eop]
    ,[md_r_eop]
INTO #inv
FROM [SizingAnalysis].[dbo].[fact_inventory]
WHERE sku_number IN (SELECT sku_number
                    FROM #selected_product)
    AND fiscalweek IN (SELECT week FROM #week);

SELECT [fiscalweek]
      ,[store_number]
      ,[sku_number]
      ,[style_number]
      ,[size]
      ,[unit_sales]
      ,[regpro_unit_sales]
      ,[md_unit_sales]
      ,[retail_sales]
      ,[regpro_retail_sales]
      ,[md_retail_sales]
      ,[gm]
      ,[regpro_gm]
      ,[md_gm]
      ,[gm_percent]
INTO #sls
FROM [SizingAnalysis].[dbo].[fact_sales]
WHERE sku_number IN (SELECT sku_number
                    FROM #selected_product)
    AND fiscalweek IN (SELECT week FROM #week);

SELECT DISTINCT fiscalweek
    ,store_number
 ,style_number
    ,sku_number
INTO #week_store_sku_size
FROM (SELECT fiscalweek
    ,store_number
 ,style_number
    ,sku_number
    FROM #inv
    UNION ALL
    SELECT fiscalweek
    ,store_number
 ,style_number
    ,sku_number
    FROM #sls) sls_inv;
SELECT *

FROM (SELECT ref.fiscalweek
     ,wr.weeknum AS week_number
     ,ref.store_number
     ,ref.style_number
     ,ref.sku_number
     ,prod.size
     ,prod.master_style
     ,parent_style_desc
     ,department
     ,department_name
     ,class
     ,class_name
     ,subclass
     ,sub_name subclass_name
     ,prod.license
     ,prod.sub_license
     ,prod.band
     ,prod.music_genre
     ,prod.music_super_genre
     ,prod.property_type
     ,prod.body_style_silhouette
     ,[unit_eop]
     ,[reg_unit_eop]
     ,[md_u_eop]
     ,[c_eop]
     ,[reg_c_eop]
     ,[md_c_eop]
     ,[r_eop]
     ,[reg_r_eop]
     ,[md_r_eop]
     ,[unit_sales]
     ,[regpro_unit_sales]
     ,[md_unit_sales]
     ,[retail_sales]
     ,[regpro_retail_sales]
     ,[md_retail_sales]
     ,[gm]
     ,[regpro_gm]
     ,[md_gm]
     ,[gm_percent]
	 ,BIS.web_flag
	 ,BIS.store_flag
	 ,Case WHEN bis.web_flag = '0' and ref.store_number < 3000 THEN 'Keep'
	  	   WHEN BIS.store_flag = '0' and ref.store_number >3000 THEN 'Keep'
		   WHEN bis.store_flag = '1' and bis.web_flag = '1' THEN 'KEEP' 
	 END as operation
FROM #week_store_sku_size ref
FULL OUTER JOIN #inv inv
    ON ref.fiscalweek = inv.fiscalweek
    AND ref.store_number = inv.store_number
    AND ref.sku_number = inv.sku_number
FULL OUTER JOIN #sls sls
    ON ref.fiscalweek = sls.fiscalweek
    AND ref.store_number = sls.store_number
    AND ref.sku_number = sls.sku_number
JOIN #selected_product prod
    ON ref.sku_number = prod.sku_number
JOIN [PAModelData].[dbo].[week number reference] wr
    ON ref.fiscalweek = wr.fiscal_week
AND ref.store_number IN (SELECT store_num FROM #store_m)
JOIN PAModelData.dbo.ht_basic_instock_list BIS
	ON BIS.sku_number = ref.sku_number) dt 
WHERE operation IS NOT NULL ;


DROP TABLE #selected_product;
DROP TABLE #week;
DROP TABLE #sls;
DROP TABLE #inv;
DROP TABLE #week_store_sku_size;
DROP TABLE #selected_product_init;


IF OBJECT_ID('tempdb..#selected_product_init','U') IS NOT NULL
       DROP TABLE #selected_product_init

IF OBJECT_ID('tempdb..#selected_product','U') IS NOT NULL
       DROP TABLE #selected_product

IF OBJECT_ID('tempdb..#week','U') IS NOT NULL
       DROP TABLE #week

IF OBJECT_ID('tempdb..#inv','U') IS NOT NULL
       DROP TABLE #inv

IF OBJECT_ID('tempdb..#sls','U') IS NOT NULL
       DROP TABLE #sls

IF OBJECT_ID('tempdb..#store_m','U') IS NOT NULL
       DROP TABLE #store_m

IF OBJECT_ID('tempdb..#week_store_sku_size','U') IS NOT NULL
       DROP TABLE #week_store_sku_size
