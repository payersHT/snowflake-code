--######################################################################
-- From documentation:This fact table holds daily on-order 
-- inventory at the item/location/supplier/purchase order/expected 
-- receipt date level. This is a daily snapshot of the on order table.
--#######################################################################

SELECT OO.PO_NUM,
       PROD.ITM_ID,
       SL.LOC_ID,
       OO.LOC_TYP_CDE,
       SUP.SUP_ID, 
       SUP.SUP_DESC,
       OO.HT_PO_TYP_CDE AS po_type,
       OO.EXP_RCT_DT AS est_rcpt_dt,
       WL.WEEK_NUM AS rctp_week_num,
       PROD.ITM_DESC,
       PROD.STY_ID,
       PROD.STY_DESC,
       PROD.SIZE_ID,
       PROD.SBC_ID,
       PROD.CLS_ID,
       PROD.DPT_ID,
       PROD.DIV_ID,
       OO.F_ORD_QTY AS on_ord_qty,
       OO.F_RCVD_QTY AS recd_qty
FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_F_INV_OO_ILD_B AS OO
INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_PRD_ITM_LU AS PROD
    ON PROD.ITM_KEY = OO.ITM_KEY
--Cant use the V_HT_DWH_D_ORG_STR_ATTR_LU does not contain the location 9999, 9997
INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_ORG_LOC_LU SL
    ON SL.LOC_KEY = OO.LOC_KEY
INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_SUP_LU AS SUP
    ON SUP.SUP_KEY = OO.SUP_KEY
INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV_V2 WL
    ON WL.DAY_KEY = OO.DAY_KEY
WHERE OO.ITM_KEY IN (SELECT DISTINCT ITM_KEY 
                  FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_HT_DWH_F_BASIC_INSTOCK_LU
                  WHERE ACTIVE = 'True')
AND OO.DAY_KEY IN ( SELECT DISTINCT max(DAY_KEY)
                    FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_F_INV_OO_ILD_B)