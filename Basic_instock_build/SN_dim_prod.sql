SELECT STY_DESC AS master_description, 
        DIV_ID AS "division",
       DIV_DESC AS "division_name",
       GRP_ID AS "group",
       GRP_DESC AS "group_name",
       DPT_ID AS "department",
       DPT_DESC AS "department_name",
       CLS_ID AS "class",
       CLS_DESC AS "class_name",
       SUP_ID AS "subclass",
       SUP_DESC AS "sub_name",
       STY_ID AS "style_number",
       STY_DESC AS "style_description",
       ITM_ID AS "sku_number",
       ITM_DESC AS "sku_description",
       STY_ID AS "Master_Style",
       SIZE_ID AS "size", 
       BAND AS "band",
       BODY_STYLE_SILHOUETTE AS "body_style_silhouette",
       BRAND as "brand",
       DTR as "dtr",
       FABRICATION AS "fabrication",
       FASHION_BASIC AS "fashion_basic",
       GRAPHIC as "graphic",
       HOLIDAY as "holiday",
       INSEAM AS "inseam",
       INTERNET_EXCLUSIVE AS "internet_exclusive",
       LICENSE AS "license",
       SUB_LICENSE as "sub_license",
       MUSIC_GENRE as "music_genre",
       MUSIC_SUPER_GENRE AS "music_super_genre",
       OCCASION_USAGE AS "occassion_usage",
       PLUS_SIZE AS "plus_size",
       PROPERTY_TYPE AS "property_type",
       SEASON_CODE AS "season",
       WEB_COLOR_DISCRIPTION AS "web_color_description",
       DW_STUDIO as "dw_studio",
       FAN_ART AS "fan_art",
       MERCHANT_TEST as "merchant_test",
       FINISH_STYLE as "finishstyle",
       LICENSE_VS_NON_LICENSED as "licensevsnon",
       HOME as "home",
       DISNEY as "disney",
       SIZE_MENU AS "sizemenu",
       GENDER as "gender",
       RCD_UPD_TS AS "create_date",
       ITEM_SUBCATEGORY as "item_sub",
       LICENSE_GROUP AS "licensegroup", 
       TARIFF_IMPACT as "tariff_impact",
       PLAN_CATEGORY AS "plan_category"
FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_PRD_ITM_UDA_LU
WHERE ITM_KEY IN (SELECT DISTINCT ITM_KEY 
                  FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_HT_DWH_F_BASIC_INSTOCK_LU )
     
       
       
       
       
       
       
       
       
       
 