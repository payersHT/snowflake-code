#loading packages
import pandas as pd
import numpy as np
import pyodbc
import sqlalchemy
import urllib
import cx_Oracle
import os
import re
import shutil
import datetime
from pandas import DataFrame
import snowflake.connector





conn_str = (
    r'Driver=SQL Server;'
    r'Server=CASQL01PC2\APP02;'
    r'Database=PAModelData;'
    r'Trusted_Connection=yes;')

cnxn = pyodbc.connect(r'Driver={SQL Server};Server=CASQL01PC2\APP02;Database=PAModelData;Trusted_Connection=yes;')
cursor = cnxn.cursor()
basic_instock = cursor.execute("""SELECT sku_number,
	   size,
	   size_index,
	   core,
	   threshold,
	   collection_id,
	   layer_sku,
	   active,
	   updated_dttm,
	   created_dttm,
	   web_flag,
	   store_flag
FROM PAModelData.dbo.ht_basic_instock_list""")                                     
         
basic_instock_df = pd.DataFrame([tuple(t) for t in basic_instock],
                  columns=('sku_number', 'size','size_index','core','threshold',
                           'collection_id','layer_sky','active','updated_dttm','created_dttm','web_flag','store_flag'))


basic_instock_df['web_flag'] = basic_instock_df['web_flag'].fillna(0)
basic_instock_df['store_flag'] = basic_instock_df['store_flag'].fillna(0)

os.chdir('C:\\Users\\payers\\desktop')
basic_instock_df.to_csv("basic_instock_load_data.csv",index = False,header=False)
# Gets the version
ctx = snowflake.connector.connect(
    user='PAYERS',
    password= '2Tn0Mmi278DSYo',
    #this can be found in the URL of snowflake website
    account = 'hottopic.east-us-2.azure',
    database='HT_SANDBOX_DEV_DB',
    schema='ALLOC_TMP',
    #This needs to be set so you can write to the staging and normal tables
    role= 'ALLOC_DEVELOPER_NONPRD' )
cs = ctx.cursor()
try:
    #Changing to the currect warehouse
    cs.execute(" USE WAREHOUSE ALLOC_RPT_NONPRD_WH")
    print(DataFrame(cs.fetchall()))
    #have to state the location of the file path, the "@%" is letting snowflake know to 
    #put it into the staging table for tables it will go to.
    cs.execute("PUT file://C:\\Users\\payers\\Desktop\\basic_instock_load_data.csv @%HT_BASIC_INSTOCK_LIST_TEMP OVERWRITE  = TRUE")
    #Copy the data into a new table form the stage table. 
    cs.execute("COPY INTO HT_BASIC_INSTOCK_LIST_TEMP FROM @%HT_BASIC_INSTOCK_LIST_TEMP;")
finally:
    cs.close()
ctx.close()

