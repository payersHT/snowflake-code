SET START_DATE = (SELECT DISTINCT WK_KEY FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_TIM_DAY_LU WHERE DAY_KEY = DATEADD(DD,-14,CURRENT_DATE()));
SET END_DATE = (SELECT DISTINCT WK_KEY FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_TIM_DAY_LU  WHERE DAY_KEY = DATEADD(DD,-7,CURRENT_DATE()));
SET MAX_WEEK = ( SELECT MAX(WEEK_NUM) FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV_V2
                                    WHERE WK_KEY BETWEEN $START_DATE AND $END_DATE );
SET MAX_WK_KY = (SELECT MAX(WK_KEY) FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV_V2
                                    WHERE WK_KEY BETWEEN $START_DATE AND $END_DATE);



WITH PROD_LIST AS (SELECT DISTINCT PROD.ITM_KEY, 
                  PROD.ITM_ID ,
                  PROD.ITM_DESC, 
                  PROD.STY_ID,
                  PROD.STY_DESC,
                  PROD.SIZE_ID
                  FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_PRD_ITM_LU PROD
                  WHERE PROD.ITM_KEY IN (SELECT DISTINCT ITM_KEY FROM   HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_HT_DWH_F_BASIC_INSTOCK_LU
                                    WHERE ACTIVE = 'True')),
                                    
    STORE_M AS (SELECT DISTINCT SA.LOC_KEY,
                SA.LOC_ID,
                CASE WHEN SA.LOC_ID IN ('4490','4499') THEN 'INT'
                     ELSE 'STR' END AS "STR_TYP_CDE_DESC"
                FROM  HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_HT_DWH_D_ORG_STR_ATTR_LU SA
                JOIN  HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_ORG_LOC_LU SL
                    ON SL.LOC_KEY = SA.LOC_KEY
                WHERE ACTIVE_LOC = 'Y'
                AND   CHN_ID = '1'),
                
    WEEK_LIST AS (SELECT DISTINCT WK_KEY,
                   WEEK_NUM
                 FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_D_TIM_DAY_LU_BRV_V2
                  WHERE WK_KEY BETWEEN $START_DATE AND $END_DATE),
                  
    INV AS (SELECT DISTINCT WK_KEY, 
            LOC_KEY, 
            ITM_KEY, 
            F_EOH_QTY,
            '1' as reg_unit_eop,
            '1' as md_u_eop
            FROM HT_SANDBOX_DEV_DB.ALLOC_DWH_V.DV_DWH_F_INV_EOH_ILW_B_V2
               WHERE ITM_KEY IN (SELECT DISTINCT ITM_KEY FROM PROD_LIST)
                AND WK_KEY IN (SELECT DISTINCT WK_KEY FROM WEEK_LIST)
                AND F_EOH_QTY <> 0),
     
   SLS AS (SELECT DISTINCT WK_KEY, 
          LOC_KEY, 
          ITM_KEY, 
          F_SLS_QTY,
          1 AS REGPRO_UNIT_SALES,
          1 AS MD_UNIT_SALES
          FROM  HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_HT_DWH_F_SLS_TXN_ITM_W
          WHERE ITM_KEY IN (SELECT DISTINCT ITM_KEY FROM PROD_LIST)
          AND WK_KEY IN (SELECT DISTINCT WK_KEY FROM WEEK_LIST)), 
          
   SLS_INV AS (SELECT DISTINCT WK_KEY, 
                LOC_KEY, 
              ITM_KEY
              FROM ( SELECT WK_KEY,
                    LOC_KEY, 
                    ITM_KEY
                    FROM INV
                    UNION ALL
                    SELECT WK_KEY, 
                    LOC_KEY, 
                    ITM_KEY 
                    FROM SLS) AS ID),
    
    SLS_INV_FINAL AS (SELECT DISTINCT REF.WK_KEY,
                        REF.ITM_KEY,
                        $MAX_WEEK - WL.WEEK_NUM + 1 AS rel_week,
                        REF.LOC_KEY,
                        SM.STR_TYP_CDE_DESC,
                        --PROD.STY_ID,
                        PROD.ITM_ID,
                        PROD.SIZE_ID,
                        PROD.STY_ID,
                        PROD.STY_DESC,
                        SLS.F_SLS_QTY,
                        INV.F_EOH_QTY
                      FROM SLS_INV AS REF
                     FULL OUTER JOIN INV AS INV
                        ON REF.WK_KEY = INV.WK_KEY
                        AND REF.LOC_KEY = INV.LOC_KEY
                        AND REF.ITM_KEY = INV.ITM_KEY
                     FULL OUTER JOIN SLS AS SLS
                        ON REF.WK_KEY = SLS.WK_KEY
                        AND REF.LOC_KEY = SLS.LOC_KEY
                        AND REF.ITM_KEY = SLS.ITM_KEY
                     JOIN PROD_LIST AS PROD
                        ON REF.ITM_KEY = PROD.ITM_KEY
                     JOIN WEEK_LIST AS WL
                        ON REF.WK_KEY = WL.WK_KEY
                     JOIN STORE_M AS SM
                        ON REF.LOC_KEY = SM.LOC_KEY) 
            



SELECT cat.last_week
	,cat.last_week_number
	,cat.style_number
	,cat.sku_number
	,cat.size
	,cat.size_index
	,cat.master_style
	,cat.master_style
	,cat.core
	,cat.collection_id
	,cat.web_flag
	,cat.store_flag
	,cat.layer_sku
	,cat.lw_eop_str_count
	,cat.llw_eop_str_count
	,cat.lw_str_u_eop
	,cat.lw_str_u_sls
	,cat.llw_str_u_eop
	,cat.llw_str_u_sls
    ,CASE WHEN lw_eop_web_count > 1 THEN 1 ELSE lw_eop_web_count END AS lw_eop_web_count
	,CASE WHEN llw_eop_web_count > 1 THEN 1 ELSE llw_eop_web_count END AS llw_eop_web_count
	,cat.lw_web_u_eop
	,cat.lw_web_u_sls
	,cat.llw_web_u_eop
	,cat.llw_web_u_sls
FROM (SELECT DISTINCT $MAX_WK_KY AS last_week,
       $MAX_WEEK as last_week_number,
       SIF.STY_ID AS style_number,
       SIF.ITM_ID as sku_number,
       SIF.SIZE_ID as size,
       BI.SIZE_INDEX as size_index,
       SIF.STY_ID as master_style,
       SIF.STY_DESC AS BI,
       BI.CORE AS core,
       BI.COLLECTION_ID AS collection_id,
       BI.WEB_FLAG AS web_flag,
       BI.STORE_FLAG AS store_flag,
       BI.LAYER_SKU AS layer_sku,
       SUM(CASE WHEN rel_week = 1 AND SIF.F_EOH_QTY > 0 AND STR_TYP_CDE_DESC = 'STR'
           THEN 1 ELSE 0 END) AS lw_eop_str_count,
           
       SUM(CASE WHEN rel_week = 2 AND SIF.F_EOH_QTY > 0 AND STR_TYP_CDE_DESC = 'STR'
            THEN 1 ELSE 0 END) AS llw_eop_str_count,
            
       SUM(CASE WHEN rel_week = 1 AND SIF.F_EOH_QTY > 0 AND STR_TYP_CDE_DESC = 'STR'
            THEN SIF.F_EOH_QTY ELSE 0 END) AS lw_str_u_eop,
       
       SUM(CASE WHEN rel_week = 1 AND SIF.F_SLS_QTY > 0 AND STR_TYP_CDE_DESC = 'STR'
            THEN SIF.F_SLS_QTY ELSE 0 END) AS lw_str_u_sls,
      
       SUM(CASE WHEN rel_week = 2 AND SIF.F_EOH_QTY > 0 AND STR_TYP_CDE_DESC = 'STR'
            THEN SIF.F_EOH_QTY ELSE 0 END) AS llw_str_u_eop,
     
       SUM(CASE WHEN rel_week = 2 AND SIF.F_SLS_QTY > 0 AND STR_TYP_CDE_DESC = 'STR'
            THEN SIF.F_SLS_QTY ELSE 0 END) AS llw_str_u_sls,
            
       SUM(CASE WHEN rel_week = 1 AND SIF.F_EOH_QTY > 0 AND STR_TYP_CDE_DESC = 'INT'
           THEN 1 ELSE 0 END) AS lw_eop_web_count,
           
       SUM(CASE WHEN rel_week = 2 AND SIF.F_EOH_QTY > 0 AND STR_TYP_CDE_DESC = 'INT'
            THEN 1 ELSE 0 END) AS llw_eop_web_count,
            
       SUM(CASE WHEN rel_week = 1 AND SIF.F_EOH_QTY > 0 AND STR_TYP_CDE_DESC = 'INT'
            THEN SIF.F_EOH_QTY ELSE 0 END) AS lw_web_u_eop,
       
       SUM(CASE WHEN rel_week = 1 AND SIF.F_SLS_QTY > 0 AND STR_TYP_CDE_DESC = 'INT'
            THEN SIF.F_SLS_QTY ELSE 0 END) AS lw_web_u_sls,
      
       SUM(CASE WHEN rel_week = 2 AND SIF.F_EOH_QTY > 0 AND STR_TYP_CDE_DESC = 'INT'
            THEN SIF.F_EOH_QTY ELSE 0 END) AS llw_web_u_eop,
     
       SUM(CASE WHEN rel_week = 2 AND SIF.F_SLS_QTY > 0 AND STR_TYP_CDE_DESC = 'INT'
            THEN SIF.F_SLS_QTY ELSE 0 END) AS llw_web_u_sls
       FROM SLS_INV_FINAL AS SIF
       INNER JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_HT_DWH_F_BASIC_INSTOCK_LU AS BI
        ON BI.ITM_KEY = SIF.ITM_KEY
       GROUP BY 
       SIF.STY_ID,
       SIF.ITM_ID,
       SIF.SIZE_ID,
       BI.SIZE_INDEX,
       SIF.STY_ID,
       SIF.STY_DESC,
       BI.CORE,
       BI.COLLECTION_ID,
       BI.WEB_FLAG,
       BI.STORE_FLAG,
       BI.LAYER_SKU) AS cat;
        
       
                
                

                                    
                              