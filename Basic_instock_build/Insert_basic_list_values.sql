INSERT INTO HT_SANDBOX_DEV_DB.ALLOC_DWH.HT_BASIC_INSTOCK_LIST (ITM_KEY,
                                                              SKU_NUM,
                                                              SIZE,
                                                              SIZE_INDEX,
                                                              CORE,
                                                              THRESHOLD,
                                                              COLLECTION_ID,
                                                              LAYER_SKU,
                                                              ACTIVE,
                                                              UPDATED_DTTM,
                                                              CREATED_DTTM,
                                                              WEB_FLAG,
                                                              STORE_FLAG)

SELECT PROD.ITM_KEY,
       BIS.SKU_NUM,
       BIS.SIZE,
       BIS.SIZE_INDEX,
       BIS.CORE,
       BIS.THRESHOLD,
       BIS.COLLECTION_ID,
       BIS.LAYER_SKU,
       BIS.ACTIVE,
       BIS.UPDATED_DTTM,
       BIS.CREATED_DTTM,
       BIS.WEB_FLAG,
       BIS.STORE_FLAG
FROM HT_SANDBOX_DEV_DB.ALLOC_TMP.HT_BASIC_INSTOCK_LIST_TEMP AS BIS
inner JOIN HT_SANDBOX_DEV_DB.ALLOC_DWH_V.V_DWH_D_PRD_ITM_UDA_LU AS PROD
    ON PROD.ITM_ID = BIS.SKU_NUM