# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Tree Diagram ###
├── Basic_instock_build \
│   ├── HT_BASIC_INSTOCK_LIST_TABLE.sql\
│   ├── HT_BASIC_INSTOCK_LIST_TEMP.sql\
│   ├── Insert_basic_list_values.sql\
│   ├── MSS_sales_inventory.sql\
│   ├── MSS_to_SF_basic_instock_data_move.py\
│   ├── SF.dc_inv.sql\
│   ├── SF_On_Order.sql\
│   ├── SF_sales_inventory.sql\
│   ├── SF_size_order.sql\
│   ├── SF_store_master.sql\
│   ├── SN_dim_prod.sql\
│   └── SN_instock_base.sql\
├── Basic_instock_update\
│   ├── insert_basic_instock_list_to_production.sql\
│   └── insert_new_data_6_25_2021.sql\
├── README.md\
├── SIZE_MENU\
│   ├── CREATE_SIZE_MENU_TABLE.sql\
│   ├── CREATE_SIZE_MENU_VIEW.sql\
│   ├── INSERT_SIZE_DATA_INTO_TABLE.sql\
│   └── transfer_size_menu_data_to_SF.py\
├── View_TIM_DAY_LU_BRV_V2.sql\
├── connecting_to_snowflake\
│   ├── CREATE TABLE test_load.sql\
│   ├── Connecting_to_SF_put_copy_function.py\
│   ├── connecting_to_snowflake.py\
│   └── testing_snowflake_connection.py\
├── sls_inv_by_week.sql\
└── temp_tables\
    ├── ht basic instock list temp.sql\
    └── ht size menu.sql\