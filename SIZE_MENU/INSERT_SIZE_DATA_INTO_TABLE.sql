INSERT INTO HT_SANDBOX_DEV_DB.ALLOC_DWH.HT_DWH_F_SIZE_MENU  (SIZE_CATEGORY_NAME,
                                                             TRUE_SIZE,
                                                             SIZE_INDEX,
                                                             DEPRICATED,
                                                             REPLACEMENT,
                                                             REPORT_SIZE )
SELECT SIZE_CATEGORY_NAME,
        TRUE_SIZE,
        SIZE_INDEX,
        DEPRICATED,
        REPLACEMENT,
        REPORT_SIZE
FROM HT_SANDBOX_DEV_DB.ALLOC_TMP.SIZE_MENU
