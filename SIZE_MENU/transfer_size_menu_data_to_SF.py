# -*- coding: utf-8 -*-
#loading packages
import pandas as pd
import numpy as np
import pyodbc
import sqlalchemy
import urllib
import cx_Oracle
import os
import re
import shutil
import datetime
from pandas import DataFrame
import snowflake.connector





conn_str = (
    r'Driver=SQL Server;'
    r'Server=CASQL01PC2\APP02;'
    r'Database=PAModelData;'
    r'Trusted_Connection=yes;')

cnxn = pyodbc.connect(r'Driver={SQL Server};Server=CASQL01PC2\APP02;Database=PAModelData;Trusted_Connection=yes;')
cursor = cnxn.cursor()
size_menu = cursor.execute("""SELECT [size_category_name]
      ,[true_size]
      ,[size_index]
      ,[depricated]
      ,[replacement]
      ,[report_size]
  FROM [PAModelData].[dbo].[size_menu]""")                                     
         
size_menu_df = pd.DataFrame([tuple(t) for t in size_menu],
                  columns=('size_category_name', 'true_size','size_index','depricated','replacement','report_size'))




os.chdir('C:\\Users\\payers\\desktop')
size_menu_df.to_csv("Size_menu_load_data.csv",index = False,header=False)
# Gets the version
ctx = snowflake.connector.connect(
    user='',
    password= '',
    #this can be found in the URL of snowflake website
    account = 'hottopic.east-us-2.azure',
    database='HT_SANDBOX_DEV_DB',
    schema='ALLOC_TMP',
    #This needs to be set so you can write to the staging and normal tables
    role= 'ALLOC_DEVELOPER_NONPRD' )
cs = ctx.cursor()
try:
    #Changing to the currect warehouse
    cs.execute(" USE WAREHOUSE ALLOC_RPT_NONPRD_WH")
    print(DataFrame(cs.fetchall()))
    #have to state the location of the file path, the "@%" is letting snowflake know to 
    #put it into the staging table for tables it will go to.
    cs.execute("PUT file://C:\\Users\\payers\\Desktop\\Size_menu_load_data.csv @%SIZE_MENU OVERWRITE  = TRUE")
    #Copy the data into a new table form the stage table. 
    cs.execute("COPY INTO SIZE_MENU FROM @%SIZE_MENU;")
finally:
    cs.close()
ctx.close()

